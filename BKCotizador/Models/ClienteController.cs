﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BKCotizador.Models
{
    public class ClienteController
    {
        public List<Cliente> getCliente(String code )
        {
            List<Cliente> Cliente = new List<Cliente>();
            SqlConnection conexion = new SqlConnection("Server=DESKTOP-15MD6K8;Database=CotizadorEnvios; integrated security = true");
            conexion.Open();
            String query = "select * from Cliente where codigo = '"+ code + "'";
            SqlCommand comando = new SqlCommand(query, conexion);
            SqlDataReader registros = comando.ExecuteReader();
            while (registros.Read())
            {
                var res = new Cliente
                {
                    id_cliente = registros["id_cliente"].ToString(),
                    nombre = registros["nombre"].ToString(),
                    apellido = registros["apellido"].ToString(),
                    codigo = registros["codigo"].ToString(),
                    direccion = registros["direccion"].ToString(),
                    telefono = registros["telefono"].ToString(),
                    nit = registros["nit"].ToString(),
                    correo = registros["correo"].ToString(),
                    id_pais = registros["id_pais"].ToString()                                                   
                };
                Cliente.Add(res);
            }
            conexion.Close();
            return Cliente;
        }
    }
}
