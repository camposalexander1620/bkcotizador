﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BKCotizador.Models
{
    public class PaisController
    {
        public List<Pais> getPais(Int32 idRegion)
        {
            List<Pais> pais = new List<Pais>();
            SqlConnection conexion = new SqlConnection("Server=DESKTOP-15MD6K8;Database=CotizadorEnvios; integrated security = true");
            conexion.Open();
            String query = "select * from Pais where id_region = "+ idRegion + "";
            SqlCommand comando = new SqlCommand(query, conexion);
            SqlDataReader registros = comando.ExecuteReader();
            while (registros.Read())
            {
                var res = new Pais
                {
                    id_region = registros["id_region"].ToString(),
                    id = Int32.Parse(registros["id"].ToString()),
                    nombre = registros["nombre"].ToString()                    
                };
                pais.Add(res);
            }
                conexion.Close();
            return pais;
        }

        public List<Region> GetRegions()
        {
            List<Region> region = new List<Region>();
            SqlConnection conexion = new SqlConnection("Server=DESKTOP-15MD6K8;Database=CotizadorEnvios; integrated security = true");
            conexion.Open();
            String query = "select * from Region";
            SqlCommand comando = new SqlCommand(query, conexion);
            SqlDataReader registros = comando.ExecuteReader();
            while (registros.Read())
            {
                var res = new Region
                {                     
                    id = Int32.Parse(registros["id"].ToString()),
                    nombre = registros["nombre"].ToString()
                };
                region.Add(res);
            }
            conexion.Close();
            return region;
        }
    }
}
