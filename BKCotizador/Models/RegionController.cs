﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BKCotizador.Models
{
    public class RegionController
    {
        public List<Region> getRegion(Int32 id)
        {
            List<Region> Region = new List<Region>();
            SqlConnection conexion = new SqlConnection("Server=DESKTOP-15MD6K8;Database=CotizadorEnvios; integrated security = true");
            conexion.Open();
            String query = "select * from Region where id = "+id +"";
            SqlCommand comando = new SqlCommand(query, conexion);
            SqlDataReader registros = comando.ExecuteReader();
            while (registros.Read())
            {
                var res = new Region
                {
                  
                    id = Int32.Parse(registros["id"].ToString()),
                    nombre = registros["nombre"].ToString()
                };
                Region.Add(res);
            }
            conexion.Close();
            return Region;
        }
    }
}
