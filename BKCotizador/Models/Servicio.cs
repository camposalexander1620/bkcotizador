﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BKCotizador.Models
{
    public class Servicio
    {
        internal string id_Servicio;

        public int id { get; set; }
        public String servicio { get; set; }
        public String tarifa { get; set; }
       
    }
}
