﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BKCotizador.Models
{
    public class Cliente
    {
        public string id_cliente { get; set; }
        public String nombre { get; set; }
        public String apellido { get; set; }
        public String codigo { get; set; }
        public String direccion { get; set; }
        public String telefono { get; set; }
        public String nit { get; set; }
        public String correo { get; set; }
        public String id_pais { get; set; }  
    }
} 
