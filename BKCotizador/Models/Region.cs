﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BKCotizador.Models
{
    public class Region
    {
        public int id { get; set; }
        public String nombre { get; set; }
    }
}
