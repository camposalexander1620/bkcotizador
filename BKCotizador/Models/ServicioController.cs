﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BKCotizador.Models
{
    public class ServicioController
    {
        public List<Servicio> GetServicioCliente(String code, Int32 idPais)
        {
            List<Servicio> Servicio = new List<Servicio>();
            System.Data.SqlClient.SqlConnection conexion = new SqlConnection("Server=DESKTOP-15MD6K8;Database=CotizadorEnvios; integrated security = true");
            conexion.Open();
            String query = "select * from cliente c " +
                "inner join Pais p on p.id = c.id_pais " +
                "inner join Servicio s on s.id_pais = p.id " +
                "where c.codigo = '"+code+"' " +
                "and p.id = " + idPais + " " +
                "and s.servicio = 'Premium'";
            SqlCommand comando = new SqlCommand(query, conexion);
            SqlDataReader registros = comando.ExecuteReader();
            while (registros.Read())
            {
                var res = new Servicio
                {                         
                    tarifa = registros["tarifa"].ToString(),
                };
                Servicio.Add(res);
            }
            conexion.Close();
            return Servicio;
        }

        public Servicio GetServiciosDefault()
        {            
            System.Data.SqlClient.SqlConnection conexion = new SqlConnection("Server=DESKTOP-15MD6K8;Database=CotizadorEnvios; integrated security = true");
            conexion.Open();
            String query = "select tarifa from Servicio where id = 13";
            SqlCommand comando = new SqlCommand(query, conexion);
            SqlDataReader registros = comando.ExecuteReader();
            Servicio serv = new Servicio();
            while (registros.Read())
            {
                var res = new Servicio
                {
                    tarifa = registros["tarifa"].ToString(),
                };
                serv = res;
            }
            conexion.Close();
            return serv;
        }
    }
}
