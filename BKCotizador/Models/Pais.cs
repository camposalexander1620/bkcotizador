﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BKCotizador.Models
{
    public class Pais
    {
        public int id { get; set; }
        public String nombre { get; set; }
        public String id_region { get; set; }
    }
}
