﻿using BKCotizador.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BKCotizador.Controllers
{
    [ApiController]
    [Route("api/cotizador")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet("{id}")]
        public IEnumerable<Pais> Get(int id)
        {
            PaisController controller = new PaisController();
            return controller.getPais(id);
        }

        [HttpGet("pais/{idRegion}")]
        public IEnumerable<Pais> GetPais(int idRegion)

        {
            PaisController controller = new PaisController();
            return controller.getPais(idRegion);
        }

        [HttpGet("region")]
        public IEnumerable<Region> GetRegions()
        {
            PaisController controller = new PaisController();
            return controller.GetRegions();
        }


        [HttpGet("servicio/Cliente/{code}/Pais/{id}")]
        public IEnumerable<Servicio> GetServicioCliente(String code, Int32 id)

        {
            ServicioController controller = new ServicioController();
            return controller.GetServicioCliente(code, id);
        }

        [HttpGet("servicio")]
        public Servicio GetServicios()
        {
            ServicioController controller = new ServicioController();
            return controller.GetServiciosDefault();
        }

        [HttpGet("Region/{id}")]
        public IEnumerable<Region> getRegion(int id)

        {
            RegionController controller = new RegionController();
            return controller.getRegion(id);
        }

        [HttpGet("Cliente/{code}")]
        public IEnumerable<Cliente> getCliente(String code)

        {
            ClienteController controller = new ClienteController();
            return controller.getCliente(code);
        }
    }
}
